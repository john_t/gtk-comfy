use common_macros::hash_map;
use gtk_comfy::*;
use std::borrow::Cow;
use std::fs;
use std::io::Write;

fn main() {
    let manual_interface = Interface {
        domain: None,
        interface: Widget {
            class: Cow::Borrowed("GtkApplicationWindow"),
            id: Some(Cow::Borrowed("window")),
            properties: hash_map! {
                Cow::Borrowed("title") => Property::String(Cow::Borrowed("My Gtk App")),
            },
            children: vec![Child {
                child_type: None,
                widget: Widget {
                    class: Cow::Borrowed("GtkButton"),
                    id: Some(Cow::Borrowed("Button")),
                    properties: hash_map! {
                        Cow::Borrowed("label") => Property::String(Cow::Borrowed("\\_Press me!")),
                        Cow::Borrowed("margin-top") => Property::String(Cow::Borrowed("12")),
                        Cow::Borrowed("margin-bottom") => Property::String(Cow::Borrowed("12")),
                        Cow::Borrowed("margin-start") => Property::String(Cow::Borrowed("12")),
                        Cow::Borrowed("margin-end") => Property::String(Cow::Borrowed("12")),
                    },
                    ..Widget::default()
                },
            }],
            ..Widget::default()
        },
    };

    println!("{}", serde_yaml::to_string(&manual_interface).unwrap());

    let interface: Interface =
        serde_yaml::from_str(&include_str!("../data/full_example.yaml")).unwrap();

    assert_eq!(manual_interface, interface);

    let mut file = fs::File::create("data/full_example.xml").unwrap();
    file.write(interface.to_string().as_bytes()).unwrap();
}
