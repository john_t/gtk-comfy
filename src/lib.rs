#![doc = include_str!("../README.md")]

use escaper::{encode_attribute, encode_minimal};
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt::{self, Display};
pub mod util;

#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct Interface<'a> {
    /// The main widget.
    pub interface: Widget<'a>,
    /// The translation domain.
    pub domain: Option<Cow<'a, str>>,
    /// Menus
    #[serde(default)]
    pub menus: Vec<Menu<'a>>,
}

impl Display for Interface<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"<?xml version="1.0", encoding="UTF-8"?>"#)?;
        match &self.domain {
            Some(domain) => write!(f, r#"<interface domain="{}">"#, encode_attribute(&domain))?,
            None => write!(f, r#"<interface>"#)?,
        }
        for menu in &self.menus {
            menu.fmt(f, "menu")?;
        }
        write!(f, "{}", self.interface)?;
        write!(f, "</interface>")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct Menu<'a> {
    /// An id
    #[serde(default)]
    pub id: Option<Cow<'a, str>>,
    /// A user-visible string to display. Only use with submenus
    #[serde(default)]
    pub label: Option<Translatable<'a>>,
    /// An icon. Only use with submenus
    #[serde(default)]
    pub icon: Option<Cow<'a, str>>,
    /// Sections
    #[serde(default)]
    pub sections: Vec<MenuSection<'a>>,
    /// Submenus
    #[serde(default)]
    pub submenus: Vec<Menu<'a>>,
}

impl<'a> Menu<'a> {
    fn fmt(&self, f: &mut fmt::Formatter, tag: &str) -> fmt::Result {
        match self.id.as_ref() {
            Some(id) => write!(f, "<{} id='{}'>", tag, encode_attribute(id))?,
            None => write!(f, "<{}>", tag)?,
        }
        if let Some(label) = self.label.as_ref() {
            label.write_as_tag(f, "attribute", "name='label'")?;
        }
        if let Some(icon) = self.icon.as_deref() {
            write!(
                f,
                "<attribute name='icon'>{}</attribute>",
                encode_minimal(icon)
            )?;
        }
        for section in &self.sections {
            write!(f, "{}", section)?;
        }
        for submenu in &self.submenus {
            submenu.fmt(f, "submenu")?;
        }
        write!(f, "</{}>", tag)
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct MenuSection<'a> {
    /// A user-visible string to use as section heading
    pub label: Option<Translatable<'a>>,

    /// A string used to determine special formatting for the section.
    /// Possible values include "horizontal-buttons", "circular-buttons"
    /// and "inline-buttons"
    #[serde(default)]
    pub display_hint: Option<Cow<'a, str>>,

    /// A string used to determine the `GtkTextDirection` to use when
    /// `display_hint` is set to "horizontal-buttons". Possible values
    /// include "rtl", "ltr" and "none"
    #[serde(default)]
    pub text_direction: Option<Cow<'a, str>>,

    /// The `MenuItem`s within the section
    #[serde(default)]
    pub items: Vec<MenuItem<'a>>,
}

impl<'a> Display for MenuSection<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<section>")?;
        if let Some(label) = self.label.as_ref() {
            label.write_as_tag(f, "attribute", "name='label'")?;
        }
        if let Some(display_hint) = self.display_hint.as_deref() {
            write!(
                f,
                "<attribute name='display-hint'>{}</attribute>",
                encode_minimal(display_hint)
            )?;
        }
        if let Some(text_direction) = self.text_direction.as_deref() {
            write!(
                f,
                "<attribute name='text-direction'>{}</attribute>",
                encode_minimal(text_direction)
            )?;
        }
        for item in &self.items {
            write!(f, "{}", item)?;
        }
        write!(f, "</section>")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default, Clone)]
pub struct MenuItem<'a> {
    /// A user-visible string to display
    pub label: Translatable<'a>,

    /// The prefixed name of the action to trigger
    pub action: Cow<'a, str>,

    /// The parameter to use when activating the action
    #[serde(default)]
    pub target: Option<Cow<'a, str>>,

    /// Names of icons that may be displayed.
    #[serde(default)]
    pub icon: Option<Cow<'a, str>>,

    /// Names of icons that may be displayed.
    #[serde(default)]
    pub verb_icon: Option<Cow<'a, str>>,

    /// Name of an action that may be used to track wether a submenu is
    /// open.
    #[serde(default)]
    pub submenu_action: Option<Cow<'a, str>>,

    /// A string used to determine when the item will be hidden.
    ///
    /// Possible values include "action-disabled", "action-missing",
    /// "macos-menubar". This is mainly useful for exported menus.
    #[serde(default)]
    pub hidden_when: Option<Cow<'a, str>>,

    /// A string used to match against the ID of a custom child added with
    /// `gtk::PopoverMenu::add_child`, `gtk::PopoverMenuBar::add_child` or
    /// in the ui file with
    ///
    /// ```yaml
    /// child_type: ID
    /// ```
    #[serde(default)]
    pub custom: Option<Cow<'a, str>>,
}

impl<'a> Display for MenuItem<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<item>")?;

        self.label.write_as_tag(f, "attribute", "name='label'")?;

        write!(
            f,
            "<attribute name='action'>{}</attribute>",
            encode_minimal(&self.action)
        )?;

        if let Some(target) = self.target.as_deref() {
            write!(
                f,
                "<attribute name='target'>{}</attribute>",
                encode_minimal(target)
            )?;
        }
        if let Some(icon) = self.icon.as_deref() {
            write!(
                f,
                "<attribute name='icon'>{}</attribute>",
                encode_minimal(icon)
            )?;
        }
        if let Some(verb_icon) = self.verb_icon.as_deref() {
            write!(
                f,
                "<attribute name='verb-icon'>{}</attribute>",
                encode_minimal(verb_icon)
            )?;
        }
        if let Some(submenu_action) = self.submenu_action.as_deref() {
            write!(
                f,
                "<attribute name='submenu-action'>{}</attribute>",
                encode_minimal(submenu_action)
            )?;
        }
        if let Some(hidden_when) = self.hidden_when.as_deref() {
            write!(
                f,
                "<attribute name='hidden-when'>{}</attribute>",
                encode_minimal(hidden_when)
            )?;
        }
        if let Some(custom) = self.custom.as_deref() {
            write!(
                f,
                "<attribute name='custom'>{}</attribute>",
                encode_minimal(custom)
            )?;
        }

        write!(f, "</item>")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct Widget<'a> {
    /// The widget's class, ie `GtkButton` or `AdwHeaderBar`
    pub class: Cow<'a, str>,
    /// The widget's id
    pub id: Option<Cow<'a, str>>,
    /// All the properties of the widget. Since they are serde flattened
    /// you don't have to include a `properties` value.
    #[serde(flatten)]
    pub properties: HashMap<Cow<'a, str>, Property<'a>>,
    /// All the children of the widget.
    #[serde(default)]
    pub children: Vec<Child<'a>>,
    /// All the pango attributes of widget. For labels.
    #[serde(default)]
    pub attributes: HashMap<Cow<'a, str>, Cow<'a, str>>,
    /// The styles of the widget.
    #[serde(default)]
    pub style: Cow<'a, [Cow<'a, str>]>,
    /// The widget's layout properties for grids.
    #[serde(default)]
    pub layout: HashMap<Cow<'a, str>, Property<'a>>,
    /// Any addition xml you wish to use.
    #[serde(default)]
    pub xml: Option<Cow<'a, str>>,
}

impl Display for Widget<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, r#"<object class="{}""#, encode_attribute(&self.class))?;
        if let Some(id) = &self.id {
            write!(f, r#" id="{}""#, encode_attribute(&id))?;
        }
        write!(f, ">")?;

        for (key, property) in &self.properties {
            property.format_as_xml_tag(key, f)?;
        }

        if !self.style.is_empty() {
            write!(f, "<style>")?;
            for style in &*self.style {
                write!(f, r#"<class name="{}"/>"#, encode_attribute(style))?;
            }
            write!(f, "</style>")?;
        }

        if !self.attributes.is_empty() {
            write!(f, "<attributes>")?;
            for (attr, val) in &self.attributes {
                write!(
                    f,
                    r#"<attribute name="{}" value="{}"/>"#,
                    encode_attribute(attr),
                    encode_attribute(val),
                )?;
            }
            write!(f, "</attributes>")?;
        }

        if !self.layout.is_empty() {
            write!(f, "<layout>")?;
            for (key, property) in &self.layout {
                write!(
                    f,
                    r#"<property name="{}">{}</property>"#,
                    encode_attribute(key),
                    property
                )?;
            }
            write!(f, "</layout>")?;
        }

        for child in &*self.children {
            write!(f, "{}", child)?;
        }

        if let Some(xml) = &self.xml {
            write!(f, "{}", xml)?;
        }

        write!(f, "</object>")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum Property<'a> {
    /// A string.
    String(Cow<'a, str>),
    /// A translatable
    Translatable(Translatable<'a>),
    /// A simple boolean value
    Bool(bool),
    /// A simple integer value
    Integer(usize),
    /// A float
    Float(f64),
    /// A widget
    Widget(Widget<'a>),
}

impl Property<'_> {
    /// Converts this into an xml tag
    pub fn format_as_xml_tag(&self, name: &str, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Translatable(string) => string.write_as_tag(
                f,
                "<property",
                &format!(r#"name="{}""#, encode_attribute(name)),
            )?,
            Self::String(string) => write!(
                f,
                r#"<property name="{}">{}"#,
                encode_attribute(name),
                encode_minimal(string)
            )?,
            Self::Bool(boolean) => write!(
                f,
                r#"<property name="{}">{}"#,
                encode_attribute(name),
                boolean
            )?,
            Self::Integer(integer) => write!(
                f,
                r#"<property name="{}">{}"#,
                encode_attribute(name),
                integer
            )?,
            Self::Float(float) => write!(
                f,
                r#"<property name="{}">{}"#,
                encode_attribute(name),
                float
            )?,
            Self::Widget(widget) => write!(
                f,
                r#"<property name="{}">{}"#,
                encode_attribute(name),
                widget
            )?,
        }
        write!(f, "</property>")
    }
}

impl Display for Property<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Translatable(string) => write!(f, "{}", encode_minimal(&string.translatable)),
            Self::String(string) => write!(f, "{}", encode_minimal(string)),
            Self::Bool(boolean) => write!(f, "{}", boolean),
            Self::Integer(integer) => write!(f, "{}", integer),
            Self::Float(float) => write!(f, "{}", float),
            Self::Widget(widget) => write!(f, "{}", widget),
        }
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct Child<'a> {
    pub child_type: Option<Cow<'a, str>>,
    #[serde(flatten)]
    pub widget: Widget<'a>,
}

impl Display for Child<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "<child ")?;
        if let Some(child_type) = &self.child_type {
            write!(f, r#"type="{}""#, encode_attribute(&child_type))?;
        }
        write!(f, ">")?;

        write!(f, "{}", self.widget)?;

        write!(f, "</child>")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Default, Clone)]
pub struct Translatable<'a> {
    #[serde(alias = "_")]
    pub translatable: Cow<'a, str>,
    #[serde(default)]
    pub context: Option<Cow<'a, str>>,
    #[serde(default)]
    pub comment: Option<Cow<'a, str>>,
}

impl<'a> Translatable<'a> {
    pub fn write_as_tag(&self, f: &mut fmt::Formatter, tag: &str, tag_attr: &str) -> fmt::Result {
        write!(f, r#"<{} translatable="true" {}"#, tag, tag_attr)?;
        if let Some(context) = self.context.as_deref() {
            write!(f, "context='{}'", encode_attribute(context))?;
        }
        if let Some(comment) = self.comment.as_deref() {
            write!(f, "comment='{}'", encode_attribute(comment))?;
        }
        write!(f, ">{}", encode_minimal(&self.translatable))?;
        write!(f, "</{}>", tag)
    }
}
