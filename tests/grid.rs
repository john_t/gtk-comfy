use gtk_comfy::Interface;

#[test]
fn test() {
    let widget: Interface = serde_yaml::from_str(include_str!("../data/grid.yaml")).unwrap();
    println!("{:#?}", widget);
    println!("{}", widget.to_string());
}
