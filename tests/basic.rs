use gtk_comfy::Widget;

#[test]
fn test_parsing() {
    let widget: Widget = serde_yaml::from_str(include_str!("../data/simple_widget.yaml")).unwrap();
    println!("{:#?}", widget);
}
